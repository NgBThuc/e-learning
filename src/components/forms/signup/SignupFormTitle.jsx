import {Stack, Typography, useTheme} from '@mui/material';
import {NavLink} from 'react-router-dom';
import BrandLogo from '../../ui/BrandLogo';

const SignupFormTitle = () => {
  const theme = useTheme();

  return (
    <Stack direction='column' alignItems='center' spacing={1}>
      <NavLink to='/'>
        <BrandLogo />
      </NavLink>
      <Typography
        sx={{fontWeight: theme.typography.fontWeightBold}}
        component='h1'
        variant='h5'
      >
        ĐĂNG KÝ
      </Typography>
    </Stack>
  );
};

export default SignupFormTitle;
