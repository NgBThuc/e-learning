import {Stack, Typography, useTheme} from '@mui/material';
import {NavLink} from 'react-router-dom';
import BrandLogo from '../../ui/BrandLogo';

const LoginFormTitle = () => {
  const theme = useTheme();

  return (
    <Stack direction='column' alignItems='center' spacing={1}>
      <NavLink to='/'>
        <BrandLogo />
      </NavLink>
      <Typography
        component='h1'
        sx={{fontWeight: theme.typography.fontWeightBold}}
        variant='h5'
      >
        ĐĂNG NHẬP
      </Typography>
    </Stack>
  );
};

export default LoginFormTitle;
