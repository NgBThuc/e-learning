import {Typography} from '@mui/material';

const FooterCopyright = () => {
  return (
    <Typography variant='body1' component='p'>
      &#169; NgBT. Đã đăng ký bản quyền
    </Typography>
  );
};

export default FooterCopyright;
