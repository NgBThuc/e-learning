import {Stack, Typography, useTheme} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import BrandLogo from '../../ui/BrandLogo';

const FooterLogo = () => {
  const theme = useTheme();
  const navigate = useNavigate();

  const handleLogoClick = () => {
    navigate('/');
  };

  return (
    <Stack
      sx={{cursor: 'pointer'}}
      onClick={handleLogoClick}
      alignItems='center'
      spacing={1}
    >
      <Stack direction='column' spacing={0.5} alignItems='center'>
        <BrandLogo />
        <Typography
          variant='h6'
          sx={{fontWeight: theme.typography.fontWeightBold}}
          component='p'
        >
          SuperLearning
        </Typography>
      </Stack>
      <Typography variant='subtitle1' component='p'>
        Giáo dục định hướng tương lai
      </Typography>
    </Stack>
  );
};

export default FooterLogo;
