import {Stack, Typography} from '@mui/material';
import {useNavigate} from 'react-router-dom';
import BrandLogo from '../../ui/BrandLogo';

const HeaderLogo = () => {
  const navigate = useNavigate();

  const handleLogoClick = () => {
    navigate('/');
  };

  return (
    <Stack
      sx={{cursor: 'pointer'}}
      direction='row'
      alignItems='center'
      spacing={1}
      onClick={handleLogoClick}
    >
      <BrandLogo />
      <Typography variant='h6' component='span' fontWeight={700}>
        SuperLearning
      </Typography>
    </Stack>
  );
};

export default HeaderLogo;
