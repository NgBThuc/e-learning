import axios from 'axios';
import {localServices} from './localServices';

const https = axios.create({
  baseURL: 'https://elearningnew.cybersoft.edu.vn/api/',
  headers: {
    TokenCybersoft: process.env.REACT_APP_API_KEY,
  },
});

https.interceptors.request.use(function (config) {
  const needBearerTokenUrl = [
    'QuanLyNguoiDung/XoaNguoiDung',
    'QuanLyNguoiDung/CapNhatThongTinNguoiDung',
    'QuanLyNguoiDung/ThemNguoiDung',
    'QuanLyNguoiDung/ThongTinNguoiDung',
    'QuanLyKhoaHoc/DangKyKhoaHoc',
    'QuanLyKhoaHoc/HuyGhiDanh',
  ];

  if (
    config.url &&
    needBearerTokenUrl.includes(config.url) &&
    (config.method === 'post' ||
      config.method === 'delete' ||
      config.method === 'put')
  ) {
    config.headers = {
      ...config.headers,
      Authorization: 'Bearer ' + localServices.user.get()?.accessToken,
    };
  }
  return config;
});
export default https;
